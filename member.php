<?php
/*
制限事項
・データファイルの項目は、""で括る必要あり→不便
*/
	error_reporting(E_ALL);
	$version = "0.24";		// Script Version
	
	date_default_timezone_set('Asia/Tokyo');
	
	header("Content-Type: text/html; charset=utf-8");
	if(TRUE)
	{
		// アドレスが登録済みでない場合
	//	if (!isset($_SERVER['PHP_AUTH_USER'])){
	//	    header('WWW-Authenticate: Basic realm="Member List"');
	//	    header('HTTP/1.0 401 Unauthorized');

	//	    die('このページを見るにはログインが必要です');
	//	}else{

	//	    if ($_SERVER['PHP_AUTH_USER'] != $user
	//	        || $_SERVER['PHP_AUTH_PW'] != $password)
	//		{
	//		        header('WWW-Authenticate: Basic realm="Private Page"');
	//		        header('HTTP/1.0 401 Unauthorized');
	//
	//		        die('このページを見るにはログインが必要です');
	//		}
	//	}
	}
	
//	$user = $_SERVER['PHP_AUTH_USER'];
//	echo "<BODY>USER=".$user."</BODY>";
	
	echo <<<EOF
<!DOCTYPE html>
<HTML>
<HEAD>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<TITLE></TITLE>
</HEAD>
<BODY>
EOF;

	$basedir = "list";		// データファイルのあるディレクトリ
	$ext = ".xml";			// QUERY_STRINGでは、拡張子以外を指定する。ここで指定する拡張子を付与したものが本当のファイル名
	$fbody = $_SERVER['QUERY_STRING'];
	
	$openname = $basedir."/".$fbody.$ext;
//	echo "FILE=".$openname."<BR>\n";
	
	// XML読み込み
	$doc = new DomDocument('1.0', 'Shift-JIS');
	
	if(($doc->Load($openname)) == FALSE)
	{
		header('HTTP/1.0 404 Not Found');
		exit(1);
	}
	
	// Get File Info
	$ftime = date("Y/m/d", filemtime($openname));
	
	// Load Define Table
	$nodeList = $doc->getElementsByTagName('define')->item(0)->childNodes;
	foreach($nodeList as $node)
	{
	  switch($node->nodeType)
	    {
	    case XML_ELEMENT_NODE:
	      $id = $node->getAttribute('id');
	      $value = $node->getAttribute('value');
	      //    	      echo $id."=[".$value."]<BR>\n";
		
	      $index[trim($id)] = $value;	// 連想配列へデータの格納（Key=項目名）
	      break;
	    case XML_TEXT_NODE:
	      //	      echo "TEXT=[".$node->wholeText."]<BR>\n";
	    default:
	      break;
	    }
	}

	// Load DATA table
	$memberCnt = 0;
	$members = $doc->getElementsByTagName('member')->item(0)->childNodes;
//	echo "Count=".$members->length."<BR>\n";
	foreach($members as $member)
	{
	  switch($member->nodeType)
	    {
	    case XML_ELEMENT_NODE:
	      $datas = $member->getElementsByTagName('cell');
	      foreach($datas as $data)
		{
		  $id = $data->getAttribute('ID');
		  $value = $data->getAttribute('value');
		  //		  echo "ID=[".trim($id)."], value=[".$value."]<BR>\n";

		  $memberData[$memberCnt][trim($id)] = $value;
		}
	      $memberCnt++;
	      break;
	    }
	}
	
//	echo "<DIV Align=right>Update: $ftime</DIV>";
	echo "<TABLE border=1>\n";
	// ファイル時刻の表示
	echo "<TR>\n";
	echo " <td colspan=".(count($index) + 1)." align=right border=0>Update: ".$ftime."</td>\n";
	echo "</TR>\n";

	// ヘッダ行の表示
	echo "<tr>\n";
	echo "<th>No.</th>\n";
	foreach($index as $i)
	{
	  echo "  <th>".$i."</th>\n";
	}
	echo "</tr>\n";

	// データの表示
	for($m = 0; $m < $memberCnt; ++$m)
	  {
	    echo "<tr>\n";
	    echo "  <td>".($m + 1)."</td>\n";
	    reset($index);
	    while($i = current($index))
	      {
		$value = $memberData[$m][key($index)];
		if($value=="") $value="　";
		echo "  <td>".$value."</td>\n";
		next($index);
	      }
	    echo "</tr>\n";
	  }
	
	echo "</TABLE>\n";
	
	// バージョン表示
	echo "<p>Script Ver.".$version."</P>\n";
echo "</BODY>";
echo "</HTML>\n";
?>
